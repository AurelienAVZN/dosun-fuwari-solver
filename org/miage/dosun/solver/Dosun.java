package org.miage.dosun.solver;

public class Dosun {
	//longueur de la grille
	public int lignes;
	//largeur de la grille
	public int colonnes;
	//tableau enregistrant les indices des cases noires
	public int[] casesNoirs;
	
	public int[] zones;
	//Représentation de la grille de dosun
	public String[][] grille;
	
	public Dosun() {
		
	}
	
	//affichage en terminal de la grille
		public void print(){
			int i = 0;	//lignes
			int j = 0;	//colonnes
			while(i != this.lignes){
				while(j != this.colonnes){
					System.out.print((this.grille[i][j])+" ");
					j++;
				}
				System.out.println("");
				j = 0;
				i++;
			}
		}
}

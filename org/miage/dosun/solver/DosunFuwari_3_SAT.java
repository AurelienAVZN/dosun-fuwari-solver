package org.miage.dosun.solver;

import org.miage.dosun.*;
import org.miage.dosun.helper.MethodHelper;

public class DosunFuwari_3_SAT{
	public String[][] grille;
	public int lignes;
	public int colonnes;
	public int[] casesNoirs;
	public int[] zones;
	public int veq;
	public int nbClauses;
	public String[] Clauses;
	public int nbVar;
	
	public DosunFuwari_3_SAT(int l, int c, int n){
		this.lignes = l;
		this. colonnes = c;
		this.veq = (this.lignes*this.colonnes*2)-1;
		this.casesNoirs = new int[n];
		this.grille = new String[this.lignes][this.colonnes];
		this.nbClauses = 0;
		this.Clauses = new String[this.nbClauses];
		this.nbVar = 0;
	}
	
	public void init(){
		int i = 0;	//curseur des lignes
		int j = 0;	//curseur des colonnes
		while(i != this.lignes){
			while(j != this.colonnes){
				this.grille[i][j] = "_";
				j++;
			}
			j = 0;
			i++;
		}
	}

	//creation et remplisage du tableau de casesNoirs avec n la taille du tableau
	public void casesNoirs(String[] vals){
		//code de benjamin (sans fenetre)
		String valeur = "";	// variable de stockage de nombre lu
		int j = 0;
		for(int i = 0; i != vals.length; i++) {
			int e = MethodHelper.valNombre(vals[i]);
			this.grille[(e-1)/(this.colonnes)][(e-1)%(this.colonnes)] = "X";
			this.casesNoirs[j] = e;
			j++;
		}		
	}

	//creation et remplissage du tableau de zones avec n la taille du tableau
	public void zones(int n){

		//code Benjamin
		this.zones = new int[(this.colonnes*this.lignes)+n-casesNoirs.length];
		//remplissage
		int h = n;	//variable pour calcul du numero de la zone en traitement
		int c = 0; 	//nombre de case dans la zone
		int i = 0;	//curseur de zones
		String	s = "";	//stock de saisie clavier
		int j = 0;	//curseur de s
int knz=0;
int[] nz={2,3,3,2,4};
int kzn=0;
String[] zn={"1 2","5 9 13","10 14 15","8 12","3 4 7 11"};
		String val = "";	//valeur d'une seule valeur
		while(n != 0){
c=nz[knz];
knz++;
s=zn[kzn];
kzn++;
			j = 0;
			//entrer des valeurs des cases dans zones
			while((c != 0)&&(j != s.length())){
				if(s.charAt(j) == ' '){	//cas d'un changement de case
					this.grille[(MethodHelper.valNombre(val)-1)/(this.colonnes)][(MethodHelper.valNombre(val)-1)%(this.colonnes)] = ""+(h-n+1);
					this.zones[i] = MethodHelper.valNombre(val);
					i++;
					val = "";
					c--;	//case suivante
				}else{
					val += s.charAt(j);
				}
				j++;	//caractere suivant
			}
			if(!(val.equals(""))){	//recuperation du dernier element de la zone
				this.grille[(MethodHelper.valNombre(val)-1)/(this.colonnes)][(MethodHelper.valNombre(val)-1)%(this.colonnes)] = ""+(h-n+1);
				this.zones[i] = MethodHelper.valNombre(val);
				i++;
				val = "";
				c--;	//case suivante
			}
			n--;	//zone suivante
			this.zones[i] = ConstHelper.finZone;	//incertion du separation de zone
			i++;
		}
	}
	
	//affichage en terminal de la grille
	public void print(){
		int i = 0;	//lignes
		int j = 0;	//colonnes
		while(i != this.lignes){
			while(j != this.colonnes){
				System.out.print((this.grille[i][j])+" ");
				j++;
			}
			System.out.println("");
			j = 0;
			i++;
		}
	}
	
	//retourne vrai si l'indice n correspond a une case noire
	public boolean estCaseNoire(int n){
		int i = 0;	//curseur dans le tableau de case noir
		while((i != this.casesNoirs.length)&&(this.casesNoirs[i] != n)){	//recherche dans casesNoirs
			i++;	//passage a l'indice suivant dans casesNoirs
		}
		return (i != this.casesNoirs.length);
	}
	
	public String produitClosesBlanc(){
		//obligation d'une case valide avec insertion des regles
		int n = 1;	//indice de la zone traitee
		int i = 0;	//pointeur de l'indice du debut de zone traiter
		int j = 0;	//curseur sur zones
		int c = 0;	//compteur du nombre monome dans la somme
		String[] ts;	//tableau des clause de la zone traiter
		int cts = 0;	//curseur sur ts
		int k = 1;		//variable permettant de switcher entre les valeurs en cas de conjonction a transformer
		int l = 0;		//curseur dans ts par apport a k
		int g = 0;
		String str = "";
		while(i != this.zones.length){
			c = 0;
			cts = 0;
			//comptage des conjonction
			while(this.zones[i+cts] != ConstHelper.finZone){
				if((this.zones[i+cts]-1 != (this.zones[i+cts]-1)%this.colonnes)&&(!estCaseNoire(this.zones[i+cts]-this.colonnes))){
					c++;
				}
				cts++;
			}
			ts = new String[MethodHelper.pow2(c)];
			cts = 0;
			//initialisation de ts
			while(cts != ts.length){
				ts[cts] = "";
				cts++;
			}
			//transformation en produit de clause
			j = 0;
			while(this.zones[i+j] != ConstHelper.finZone){
				if((this.zones[i+j]-1 == (this.zones[i+j]-1)%this.colonnes)||(estCaseNoire(this.zones[i+j]-this.colonnes))){
					cts = 0;
					while(cts != ts.length){
						ts[cts] += " "+this.zones[i+j];
						cts++;
					}
				}else{
					k = 1;
					g = j;
					while(c != 0){
						cts = 0;
						while(cts != ts.length){
							l = 0;
							while(l != ts.length/(2*k)){
								ts[cts] += " "+this.zones[i+g];
								l++;
								cts++;
							}
							while(l != ts.length/k){
								ts[cts] += " "+(this.zones[i+g]-this.colonnes);
								l++;
								cts++;
							}
						}
						k = k*2;
						c--;
						g++;
					}
				}
				j++;
			}																			
			c = 0;
			while(c != ts.length){
				str += ts[c] + "" + '\n';
				c++;
			}
			i += j;	//saut a la fin de la zone
			i++;	//passage a la zone suivante
			n++;
		}
		//traitement de l'unicite en cas de validitee d'une case
		i = 0;	//curseur de deplacement dans zones
		j = 0;	//pointeur de comparaison dans la zone
		n = 1;
		while(i != this.zones.length){
			j = i+1;
			while((j != this.zones.length)&&(this.zones[j] != ConstHelper.finZone)){
				str += "-"+this.zones[i]+" -"+this.zones[j]+""+'\n';
				j++;
			}
			i++;
			if((i != this.zones.length)&&(this.zones[i] == ConstHelper.finZone)){
				n++;
				i++;
				
			}
		}
		return str;
	}
	public String produitClosesNoir(){
		//obligation d'une case valide avec insertion des regles
		int n = 1;	//indice de la zone traitee
		int i = 0;	//pointeur de l'indice du debut de zone traiter
		int j = 0;	//curseur sur zones
		int c = 0;	//compteur du nombre monome dans la somme
		String[] ts;	//tableau des clause de la zone traiter
		int cts = 0;	//curseur sur ts
		int k = 1;		//variable permettant de switcher entre les valeurs en cas de conjonction a transformer
		int l = 0;		//curseur dans ts par apport a k
		int g = 0;
		String str = "";
		while(i != this.zones.length){
			c = 0;
			cts = 0;
			//comptage des conjonction
			while(this.zones[i+cts] != ConstHelper.finZone){
				if((this.colonnes*this.lignes - this.zones[i+cts] != (this.colonnes*this.lignes - this.zones[i+cts])%this.colonnes)&&(!estCaseNoire(this.zones[i+cts]+this.colonnes))){
					c++;
				}
				cts++;
			}
			ts = new String[MethodHelper.pow2(c)];
			cts = 0;
			//initialisation de ts
			while(cts != ts.length){
				ts[cts] = "";
				cts++;
			}
			//transformation en produit de clause
			j = 0;
			while(this.zones[i+j] != ConstHelper.finZone){
				if((this.colonnes*this.lignes - this.zones[i+j] == (this.colonnes*this.lignes - this.zones[i+j])%this.colonnes)||(estCaseNoire(this.zones[i+j]+this.colonnes))){
					cts = 0;
					while(cts != ts.length){
						ts[cts] += " "+(this.zones[i+j]+this.colonnes*this.lignes);
						cts++;
					}
				}else{
					k = 1;
					g = j;
					while(c != 0){
						cts = 0;
						while(cts != ts.length){
							l = 0;
							while(l != ts.length/(2*k)){
								ts[cts] += " "+(this.zones[i+g]+this.colonnes*this.lignes);
								l++;
								cts++;
							}
							while(l != ts.length/k){
								ts[cts] += " "+(this.zones[i+g]+this.colonnes+this.colonnes*this.lignes);
								l++;
								cts++;
							}
						}
						k = k*2;
						c--;
						g++;
					}
				}
				j++;
			}
			//affichage des clauses stockees en ts
			c = 0;
			while(c != ts.length){
				str += ts[c] + "" + '\n';
				c++;
			}
			i += j;	//saut a la fin de la zone
			i++;	//passage a la zone suivante
			n++;
		}
		//traitement de l'unicite en cas de validitee d'une case
		i = 0;	//curseur de deplacement dans zones
		j = 0;	//pointeur de comparaison dans la zone
		n = 1;
		while(i != this.zones.length){
			j = i+1;
			while((j != this.zones.length)&&(this.zones[j] != ConstHelper.finZone)){
				str += "-"+(this.zones[i]+this.colonnes*this.lignes)+" -"+(this.zones[j]+this.colonnes*this.lignes)+""+'\n';
				j++;
			}
			i++;
			if((i != this.zones.length)&&(this.zones[i] == ConstHelper.finZone)){
				n++;
				i++;
			}
		}
		return str;
	}
	//Ecriture des clause d'unicité d'une boule dans une meme case (une boule blanche ne peut pas etre sur la meme case qu'une boule noire et vice versa)
	public String produitClosesBN(){
		int i = 0;//curseur sur les numero des cases
		String str = "";//inisatialisation du string de resultat
		//boucle faite en modele 2 (l'indice de la premiere case est 1)
		while(i != this.colonnes*this.lignes){
			i++;
			if(!estCaseNoire(i)){//on ne traite que les cases pouvant etre remplit par une boule
				str += "-"+i+" -"+(i+this.colonnes*this.lignes)+""+'\n';//les variables concernants les boules blanche aux indices de case i sont: i
																		//les variables concernants les boules noires aux indices de case i sont: i+(nb case total)
			}
		}
		return str;
	}
	
	// renvoie le nombre de mot (chaine de caractere separer par un espace) contenue dans s
	public int taille(String s){
		int n = 0;//nombre de mot
		int i = 0;//curseur sur s
		//suppression des espaces au debut de s
		while(i != s.length() && s.charAt(i) == ' '){
			i++;
		}
		while(i != s.length()){
			if(s.charAt(i) == ' '){
				n++;
			}
			i++;
		}
		//rajout du dernier mot de la chaine (non lu dans la boucle car ne finissant pas par un espace)
		if(s.length() != 0){
			n++;
		}
		return n;
	}
	//creation d'un tableau de string constituer de chaque mot present dans s
	public String[] indexation(String s){
		String[] t = new String[taille(s)];
		int j = 0;//curseur sur le tableau
		int i = 0;//curseur sur s
		String v = "";//mot en cours de traitement
		//suppression des espaces au debut de s
		while(i != s.length() && s.charAt(i) == ' '){
			i++;
		}
		while(i != s.length()){
			if(s.charAt(i) == ' '){
				//cas de fin de mot
				t[j] = v;//stockage du mot traite
				j++;//placement a la premiere case vide du tableau
				v = "";//reinitailisation de la variable
			}else{
				v += s.charAt(i);//continuation de lecture du mot
			}
			i++;//avancement dans s
		}
		//stockage du dernier mot de s
		if(s.length() != 0){
			t[j] = v;
		}
		return t;
	}
	
	public void ajouteClauses(String s){
		nbClauses++;
		String[] ts = new String[nbClauses];
		int i = 0;
		while (i!= Clauses.length){
			ts[i] = Clauses[i];
			i++;
		}
		ts[i] = s;
		Clauses = ts;
	}
	
	//transformation de la clause n-SAT en 3-SAT
	public String troisSAT(String s){
		String result = "";//variable de resultat
		int i = 0;//curseur sur t
		int m = 0;//valeur initiale de veq (pour un traitement distaint de la premiere boucle du while)
		String[] t = indexation(s);//recuperation des mots dans un tableau
		if(taille(s) <= 3){
			//la clause est deja 3-SAT
			result = s+'\n';
			ajouteClauses(s);
		}else{
			this.veq += 2;//augmentation de 2 pour eviter l'utilisation consecutive d'une meme variable
			m = this.veq;
			result = t[0] +" "+ t[1] +" "+ this.veq;//premiere transformation (traitement des deux premiere variable)
			ajouteClauses(result);
			result += ""+'\n';
			i = 2;
			//boucle sur tout t sauf les deux premiers car deja traitee et les deux dernier car traitement different
			while((i+2) != t.length){
				s = (this.veq+1) +" "+ t[i] +" "+ (this.veq+2);//traitement de la prochaine variable
				ajouteClauses(s);
				result += s+'\n';
				
				//clauses:  veq <=>(t[i] | veq+2)
				s = (-this.veq) +" "+ t[i] +" "+  (this.veq+2);
				ajouteClauses(s);
				result += s+'\n';
				s = this.veq + " " + "-"+t[i];
				ajouteClauses(s);
				result += s+'\n';
				s = this.veq + " " +(-this.veq-2);
				ajouteClauses(s);
				result += s+'\n';

				if(this.veq == m){
					//cas du traitement sur la troisieme donnee
					//clauses:  veq+1 <=>(t[0] | t[1])
					s = (-this.veq-1) +" "+ t[0] +" "+ t[1];
					ajouteClauses(s);
					result += s+'\n';
					s = (this.veq+1) +" "+ "-"+t[0];
					ajouteClauses(s);
					result += s+'\n';
					s =  (this.veq+1) +" "+ "-"+t[1];
					ajouteClauses(s);
					result += s+'\n';
				}else{
					//clauses:  veq+1 <=>(veq-1 | t[i-1])
					s = (-this.veq-1) +" "+ (this.veq-1) +" "+ t[i-1];
					ajouteClauses(s);
					result += s+'\n';
					s =  (this.veq+1) +" "+ (-this.veq+1);
					ajouteClauses(s);
					result += s+'\n';
					s =  (this.veq+1) +" "+ "-"+t[i-1];
					ajouteClauses(s);
					result += s+'\n';
				}
				
				this.veq += 2;
				i++;
			}
			//traitement de la derniere donnee
			s = (this.veq+1) +" "+ t[i] +" "+ t[i+1];
			ajouteClauses(s);
			result += s+'\n';
			
			//clauses:  veq <=>(t[i] | t[i+1])
			s = (-this.veq) +" "+ t[i] +" "+ t[i+1];
			ajouteClauses(s);
			result += s+'\n';
			s = this.veq +" "+ "-"+t[i];
			ajouteClauses(s);
			result += s+'\n';
			s = this.veq +" "+ "-"+t[i+1];
			ajouteClauses(s);
			result += s+'\n';
			
			if(this.veq == m){
				//cas du traitement pour la troisieme donnee
				//clauses:  veq+1 <=>(t[0] | t[1])
				s = (-this.veq-1) +" "+ t[0] +" "+ t[1];
				ajouteClauses(s);
				result += s+'\n';
				s = (this.veq+1) +" "+ "-"+t[0];
				ajouteClauses(s);
				result += s+'\n';
				s = (this.veq+1) +" "+ "-"+t[1];
				ajouteClauses(s);
				result += s+'\n';
			}else{
				//clauses:  veq+1 <=>(veq-1 | t[i-1])
				s = (-this.veq-1) +" "+ (this.veq-1) +" "+ t[i-1];
				ajouteClauses(s);
				result += s+'\n';
				s = (this.veq+1) +" "+ (-this.veq+1);
				ajouteClauses(s);
				result += s+'\n';
				s = (this.veq+1) +" "+ "-"+t[i-1];
				ajouteClauses(s);
				result += s+'\n';
			}
		}
		return result;
	}
		
	//initialise nbVar
	public void nbVar(){
		this.nbVar = (this.veq+1)-(this.casesNoirs.length*2);
	}
	
//main
	public static void main(String[] args){
		String s = "";
		String total = "";
//		System.out.println("nb lignes:");
//		int l = Entree.lireInt();
int l=4;
//		System.out.println("nb colonnes:");
//		int c = Entree.lireInt();
int c=4;
		DosunFuwari_3_SAT d = new DosunFuwari_3_SAT(l,c,2);
		d.init();
		System.out.println("");
		d.print();
		System.out.println("");
//		System.out.println("nb case noire");
//		int cn = Entree.lireInt();
int cn=2;
		//d.casesNoirs("6 16");
		System.out.println("");
		d.print();
		System.out.println("");
//		System.out.println("nb zones");
//		int z = Entree.lireInt();
int z=5;
		d.zones(z);
		System.out.println("");
		d.print();
		System.out.println("");
		System.out.println("closes blanches");
//int pute = Entree.lireInt();
		s = d.produitClosesBlanc();
		total += s;
		System.out.println(s);
		System.out.println("closes noires");
//pute = Entree.lireInt();
		s = d.produitClosesNoir();
		total += s;
		System.out.println(s);
		System.out.println("closes noires&blanches");
//pute = Entree.lireInt();
		s = d.produitClosesBN();
		total += s;
		System.out.println(s);
		
		System.out.println("Valeur des clause en 3-SAT:");
//pute = Entree.lireInt();
		String cl = "";
		int i = 0;
		while(i != total.length()){
			while(total.charAt(i) != '\n'){
				cl += total.charAt(i);
				i++;
			}
			s = d.troisSAT(cl);
			cl = "";
			System.out.print(s);
			i++;			
		}
		d.nbVar();
		System.out.println(d.nbVar);
	}
}

package org.miage.dosun;

import java.awt.Color;
import java.util.Locale;

public final class ConstHelper {
	//Path des assets
	public static final String AssetsHtml = "./Assets/html";
	public static final String AssetsImg = "./Assets/img";
	public static final String AssetsLang = "./Assets/lang";
	public static final String LangUtil = Locale.getDefault().toString();
	
	public static final String EmptyString = "";
	public static final String CommandeSolveur = "java -jar org.ow2.sat4j.core-2.3.6-SNAPSHOT.jar ";
	public static final int finZone = -5;
	public static final String NouvelleGrille = "Nouvelle grille";
	public static final String FileNotFoundText = "<html><center><p>Fichier non trouve</p></center></html>";
	public static final Color BackGroundColor = new Color(192, 192, 192);
	//A mettre dans une enum
	public static final String ErreurText = "Erreur";
	public static final String InformationText = "Information";

}

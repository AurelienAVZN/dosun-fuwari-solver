package org.miage.dosun.ui;

import javax.swing.JFrame;
import org.miage.dosun.ConstHelper;
import org.miage.dosun.helper.MethodHelper;
import org.miage.dosun.solver.DosunFuwari_3_SAT;
import org.miage.dosun.ui.extensions.*;
import org.miage.dosun.ui.helper.*;
import org.miage.dosun.ui.windows.Affichage;

import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;

public class Fenetre extends Frame {
	private static final long serialVersionUID = 778713872381634127L;
	private Menu menu = new Menu();
    public String Adresse = null;//Chemin ou nom d'un fichier .txt
    public StringBuilder clause; //Pour enregistrer les clauses avant écriture du fichier Dimacs
    public String Sol;
    public int nbZones; //Enregistrement des paramètres de la grille
    public DosunFuwari_3_SAT DF; //Construteur de la classe DosunFuwari_3_SAT
    public Affichage aff;

    public Fenetre(){
    	super("Dosun-Fuwari / Projet Logique");
        setJMenuBar(menu);
        setSize(900,700);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new GridLayout(1,2));
        this.getContentPane().setBackground(ConstHelper.BackGroundColor);
        this.setVisible(true);

        menu.ItemM11().addActionListener(new ActionListener(){
        //Création d'un grille de Dosun à partir des paramètres rentrés par l'utilsateur
            public void actionPerformed(ActionEvent arg0){
                JFrame J = new Frame(ConstHelper.NouvelleGrille);
                JButton B = new JButton("Valider");
                Panel P1 = new Panel();
                Panel P2 = new Panel();
                Panel P3 = new Panel();
                Panel P4 = new Panel();
                Panel panLongueur = new Panel(new Dimension(220, 60));
                Panel panLargeur = new Panel(new Dimension(220, 60));
                Panel panNBCgrise = new Panel(new Dimension(220, 60));
                Panel panNBzone = new Panel(new Dimension(220, 60));
            //JTextField permettra à l'utilisateur de rentrer les paramètres voulues
                TextField longueur = new TextField();
                TextField largeur = new TextField();
                TextField nbcgrise = new TextField();
                TextField nbzone = new TextField();
                panNBCgrise.setBorder(BorderFactory.createTitledBorder("Nombres de case grises"));
                panNBzone.setBorder(BorderFactory.createTitledBorder("Nombre de zones"));
                P1.setBorder(BorderFactory.createTitledBorder("Dimension Dosun"));
                P1.setLayout(new GridLayout(1,2));
                P2.setLayout(new GridLayout(1,2));
                P3.setLayout(new BoxLayout(P3, BoxLayout.LINE_AXIS));
            //Intégration des différents elts
                panLongueur.addComponent(new JLabel("Longueur : "), longueur);
                panLargeur.addComponent(new JLabel("Largeur : "), largeur);
                panNBCgrise.addComponent(new JLabel("NB cases : "), nbcgrise);
                panNBzone.addComponent(new JLabel("NB zones : "), nbzone);
                P1.addComponent(panLongueur, panLargeur);
                P2.addComponent(panNBCgrise, panNBzone);
                P3.add(B);
                P4.addComponent(P1, P2, P3);
            //Paramètre de la fenetre principal
                J.getContentPane().add(P4);
                J.setSize(500,225);
                J.setVisible(true);
                B.addActionListener(new ActionListener(){
                //Création de la grille et demande les cases noires à l'utilisateur
                    public void actionPerformed(ActionEvent arg0){
                    //Récuperation des valeurs rentrées dans un tableau
                        nbZones = Integer.valueOf(nbzone.getText());//nb zones
                    //Appel du constructeur de la grille
                        DF = new DosunFuwari_3_SAT(Integer.valueOf(longueur.getText()), 
                        		Integer.valueOf(largeur.getText()), Integer.valueOf(nbcgrise.getText()));
                    //Fermeture de la fenetre
                        J.dispose();
                    //Définition des nouvelles variables
                        Panel PCG = new Panel();
                        Panel PCG1 = new Panel(new Dimension(220, 60));
                        Panel PCG2 = new Panel();
                        TextField JTCG = new TextField();
                        JButton B = new JButton("Valider");
                        PCG.setLayout(new GridLayout(2,1));
                        PCG2.add(B);
                        PCG1.addComponent(new JLabel("Numero des cases (S�parateur ;) :"), JTCG);
                        PCG.addComponent(PCG1, PCG2);
                        JFrame JCG = new Frame("Cases grises");
                        JCG.getContentPane().add(PCG);
                        JCG.setSize(500,225);
                        JCG.setVisible(true);
                    //Définition de l'action lorsque l'utilisateur clique sur le bouton
                        B.addActionListener(new ActionListener(){
                            public void actionPerformed(ActionEvent arg0){
                            //Récuperation des cases dans un string
                                JCG.dispose();
                                DF.init();
                                DF.casesNoirs(JTCG.getText().split(";"));
                                DF.print();
                                Panel PCG = new Panel();
                                JPanel PCG1 = new Panel(new Dimension(220, 60));
                                JPanel PCG2 = new Panel();
                                Panel PCG3 = new Panel();
                                JLabel LCG = new JLabel("Zone numero 1 (separateur ;) :");
                                JLabel LCG1 = new JLabel("Nombre de cases de la zone 1:");
                                TextField JTCG = new TextField();
                                TextField JTCG1 = new TextField();
                                JButton B = new JButton("Valider");
                                int i = nbZones; //<=> i = nbzones
                            //Positionnement des elts
                                PCG.setLayout(new GridLayout(3,1));
                            //Integration des elts
                                PCG2.add(B);
                                PCG1.add(LCG);
                                PCG1.add(JTCG);
                                PCG3.addComponent(LCG1, JTCG1);
                                PCG.addComponent(PCG3, PCG1, PCG2);
                                JFrame JCG = new Frame("Cases grises");
                                JCG.getContentPane().add(PCG);
                                JCG.setSize(500,225);
                                JCG.setVisible(true);
                                DF.zones = new int[(DF.colonnes*DF.lignes)+ i - DF.casesNoirs.length];
                            //Définition de l'action lorsque l'utilisateur clique sur le bouton
                                B.addActionListener(new ActionListener(){
                                //Initialisation des variables
                                    int c = 0;
                                    int t = 0;
                                    int i = 0;
                                    int zone = 0;
                                    String s;
                                    String val = "";
                                    public void actionPerformed(ActionEvent arg0){
                                    //Il s'agit de la fonction zone de la classe DosunFuwari_3_SAT
                                    //Elle a été intégrée dans la classe fenetre car il y avait
                                    //des problemes de communication entre les classes
                                        if (zone == nbZones-1){//cas ou zone = nbzone-1
                                        //Je récupere les cases dans un string et le nombre dans un int
                                            s = JTCG.getText();
                                            c = Integer.valueOf(JTCG1.getText());
                                        //Changement du texte affiché dans la fenetre
                                            LCG1.setText("Nombre de cases de la zone "+ (t+2) + ":");
                                            LCG.setText("Zone numero "+ (t+2) + " (separateur ;) :");
                                        //Incrementation
                                            zone++;
                                            t++;
                                        //Schéma de parcours partiel modele 1
                                            int j = 0;
                                            int intVal;
                                            while((c != 0) && (j != s.length())){
                                                if(s.charAt(j) == ' '){
                                                	intVal = Integer.valueOf(val);
                                                    DF.grille[(intVal-1)/(DF.colonnes)][(intVal-1)%(DF.colonnes)] = ""+zone;
                                                    DF.zones[i] = intVal;
                                                    i++;
                                                    val = "";
                                                    c--;
                                                }else{
                                                    val += s.charAt(j);
                                                }
                                                j++;
                                            }
                                            if(!(val.equals(ConstHelper.EmptyString))){
                                            	intVal = Integer.valueOf(val);
                                                DF.grille[(intVal-1)/(DF.colonnes)][(intVal-1)%(DF.colonnes)] = ""+zone;
                                                DF.zones[i] = intVal;
                                                i++;
                                                val = "";
                                                c--;
                                            }
                                            DF.zones[i] = ConstHelper.finZone;
                                            i++;
                                            JCG.dispose();
                                            DF.print();
                                        }else{ //Cas où zone != nbzone-1
                                        //Récuperation des cases dans un string et du nombre dans un int
                                            s = JTCG.getText();
                                            c = Integer.valueOf(JTCG1.getText());
                                        //Vidage des JTextField
                                            JTCG.setText("");
                                            JTCG1.setText("");
                                        //Cangement du texte affiché dans la fenetre
                                            LCG1.setText("Nombre de cases de la zone "+ (t+2) + ":");
                                            LCG.setText("Zone numero "+ (t+2) + ":");
                                        //Incrementation des variabless zone et t
                                            zone++;
                                            t++;
                                        //Schéma de parcours partiel modele 1
                                            int j = 0;
                                            while((c != 0) && (j != s.length())){
                                            //On lit le String s jusqu'à qu'on obtient un espace
                                            //On récupère la valeur obtenu et on l'ajoute à la grille
                                                if(s.charAt(j) == ' '){
                                                    DF.grille[(Integer.valueOf(val)-1)/(DF.colonnes)][(Integer.valueOf(val)-1)%(DF.colonnes)] = ""+(nbZones-(nbZones-zone));
                                                    DF.zones[i] = Integer.valueOf(val);
                                                    i++;
                                                    val = "";
                                                    c--;
                                                }else{
                                                    val += s.charAt(j);
                                                }
                                                j++;
                                            }
                                            if(!val.equals("")){
                                                DF.grille[(Integer.valueOf(val)-1)/(DF.colonnes)][(Integer.valueOf(val)-1)%(DF.colonnes)] = ""+(nbZones-(nbZones-zone));
                                                DF.zones[i] = Integer.valueOf(val);
                                                i++;
                                                val = "";
                                                c--;
                                            }
                                            DF.zones[i] = ConstHelper.finZone;
                                            i++;
                                        //Affichage du résultat à chaque nouvelle zone indiqué par l'utilisateur (console)
                                            DF.print();
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

    //Création du fichier Dimacs associé à a grille
        menu.ItemM31().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                try{
                    clause = new StringBuilder();
                    int i = 0;
                    String cl = ConstHelper.EmptyString;
                    //Création des clauses N_SAT
                    clause.append(DF.produitClosesBlanc()).append(DF.produitClosesNoir()).append(DF.produitClosesBN());
                    //On réucpere les clauses une à une
                    while(i != clause.length()){
                    	char caractere = clause.charAt(i);
                        if (i == 0){
                            if (caractere != ' '){
                            	cl += caractere;
                            }
                            i++;
                        }
                        else{
                            while(i != clause.length() && caractere != '\n'){
                                if ((caractere != ' ') || (clause.charAt(i-1) != '\n')) {
                                    cl += caractere;
                                }
                                i++; 
                                caractere = clause.charAt(i);
                            }
                            DF.troisSAT(cl);
                            cl = ConstHelper.EmptyString;
                            i++;
                        }
                    }
                    int var = DF.nbVar = (DF.veq+1)-(DF.casesNoirs.length*2); //nombre de variables utilisés
                    String Comm = "c Fichier Dimacs"; //
                    String donnees = "c " + DF.lignes + " " + DF.colonnes + " " + DF.casesNoirs + " " + DF.zones;
                    String donneesD = "p cnf" + " " + var + " " + DF.nbClauses;
                    try (PrintWriter writer = new PrintWriter("F_Dimacs/GrilD.txt")){
                    	writer.println(Comm);
                    	writer.println(donnees);
                    	writer.println(donneesD);
                    	for(i = 0; i != DF.Clauses.length; i++) {
                    		writer.print(DF.Clauses[i]);
                        	writer.println(" 0");
                    	}
                    }
                }
                catch(Exception e){ UIHelper.showErrorFrame("Failure", e); }
            }
        });

        //Création de la commande et du fihcier executable lancement du solveur
        menu.ItemM32().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
            	File GrilD = UIHelper.OpenFile();
            	if(GrilD == null) {
            		Message.OperationCanceled(null, "CreateSolveur");
            	}
            	else {
            		//Ouverture d'une fenetre afin de choisir le fichier Dimacs
                    try (PrintWriter writer = new PrintWriter("F_Dimacs/solveur.bat")){
                    	writer.print(ConstHelper.CommandeSolveur); writer.println(GrilD.getName());
                        writer.print("pause");
                    }
                    catch(Exception e){ UIHelper.showErrorFrame("Failure", e); }
            	}
            }
        });

    //Chargement du fichier résultat et affichage de la solution
        menu.ItemM33().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
            	BufferedInputStream bis = null;
        		StringWriter sw = new StringWriter();
            	try{
            		File GrilSol = UIHelper.OpenFile();
            		bis = new BufferedInputStream(new FileInputStream(GrilSol));
            		String trans;
            		Sol = "";
                    int b;
                    while((b = bis.read()) != -1){
                        sw.write(b);
                        sw.flush();
                    }
                    trans = sw.toString();
                    int i = 1; //il faut l'initialiser à 1
                    while (i != trans.length()){
                        if (trans.charAt(i) == '\n'){ i++; }
                        else if (Character.toUpperCase(trans.charAt(i)) == 'C' || trans.charAt(i) == 's'){
                            while (trans.charAt(i) != '\n'){ i++; }
                        }
                        else{
                            i += 2;
                            while (trans.charAt(i) != '\n'){
                                Sol += trans.charAt(i);
                                i++;
                            } 
                            break;
                        }
                    }
                }
                catch(IOException e){ UIHelper.showErrorFrame("Failure", e); }
                finally {
                	MethodHelper.DisposeSafe(sw);
                	MethodHelper.DisposeSafe(bis);
                }
                //aff = new Affichage(CasesN, Sol, Grille);
            }
        });
    }
}

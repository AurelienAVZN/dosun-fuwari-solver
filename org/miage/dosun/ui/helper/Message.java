package org.miage.dosun.ui.helper;

import java.awt.Component;

import javax.swing.JOptionPane;

import org.miage.dosun.ConstHelper;

public class Message{
	public int retour;
	public String entree;

	public Message(int n){
	//Envoie un message
		if (n == 0){//Dans le cas d'un fichier non trouvé
			JOptionPane.showMessageDialog(null, ConstHelper.FileNotFoundText, ConstHelper.ErreurText, JOptionPane.INFORMATION_MESSAGE);
		}
		else if (n == 1){//Dans le cas D'un fromat de fichier non prise en compte
			JOptionPane.showMessageDialog(null,"<html><p>Format fichier non prise en compte !<br>Veuillez choisir un fichier .txt</br></p></html>", ConstHelper.ErreurText, JOptionPane.ERROR_MESSAGE);
		}
		else if (n == 2){// Si l'utilisateur créer une nouvelle grille sans avoir enregistrer la précédente
			this.retour = JOptionPane.showConfirmDialog(null, "<html><p>La validation entrainera la suppresion de la grille actuelle!<br>Voulez-vous continuez ?</br></p></html>", "Attention", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		}
		else if (n == 3){//Dans le cas où l'uitilisateur n'as pas définie de zone pour la grilleS
			JOptionPane.showMessageDialog(null, "<html><p>Vous n'avez pas defini de zones !!<br>Au minimun une zone est obligatoire !</br></p></html>", ConstHelper.ErreurText, JOptionPane.ERROR_MESSAGE);
		}
		else if (n == 4){//Dans le cas où l'utilisaeur essaye d'ouvrir deux fois une même fenetre
			JOptionPane.showMessageDialog(null, "<html><p>La fenetre que vous avez demandez est deja ouverte !!</p></html>", ConstHelper.InformationText, JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public static void OperationCanceled(Component parent, String operationName) {
		JOptionPane.showMessageDialog(parent, 
				new StringBuilder().append("<html><p>Operation ").append(operationName).append(" annul�e du aux manques d'information</p></html>").toString(), 
				ConstHelper.InformationText, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void main(String[] args) {
		Message msg = new Message(2);
	}
}
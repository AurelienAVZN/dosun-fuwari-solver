package org.miage.dosun.ui.helper;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public final class UIHelper {
	public static void showErrorFrame(String message, Exception e){
		StringBuilder builder = new StringBuilder();
		builder.append(message).append("\n");
        for (int i = 0; i < e.getStackTrace().length; i++) {
        	builder.append(e.getStackTrace()[i]).append("\n"); 
        }
        JOptionPane.showMessageDialog(null, builder.toString(), "Error", JOptionPane.ERROR_MESSAGE, null);
    }
	
	public static File OpenFile() {
        JFileChooser dialogue = new JFileChooser(new File("."));
        if (dialogue.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
        	return dialogue.getSelectedFile();
        }
        return null;
	}
	
	public static boolean SaveFile() {
		return false;
		
	}
}

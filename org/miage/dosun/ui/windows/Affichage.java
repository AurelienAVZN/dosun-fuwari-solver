package org.miage.dosun.ui.windows;
import java.util.StringTokenizer;

import org.miage.dosun.solver.DosunFuwari_3_SAT;

public class Affichage{
    public DosunFuwari_3_SAT DF1;

    public Affichage(String s1, String s2, int[] t1){
    	String sgrill = "";
    	int i = 0;
    	int j = 0;
    	DF1 = new DosunFuwari_3_SAT(t1[0],t1[1],t1[2]);
    	DF1.init();
    	//DF1.casesNoirs(s1);

    	StringTokenizer st = new StringTokenizer(s2);
    	while (st.hasMoreTokens()){
    		int p = Integer.valueOf(st.nextToken());
    		int nbc = (DF1.lignes*DF1.colonnes)*2;
    		if(Math.abs(p) <= (nbc/2)){//boule blanche
    			if (p > 0){
    				DF1.grille[(p-1)/(DF1.colonnes)][(p-1)%(DF1.colonnes)] = "B";
    			}
    		}else{
    			if(Math.abs(p) <= nbc){
    				if(p > 0){
    					int c = Math.abs(p)-(DF1.lignes*DF1.colonnes);
    					DF1.grille[(c-1)/(DF1.colonnes)][(c-1)%(DF1.colonnes)] = "N";
    				}
    			}
    		}
    	}
    	DF1.print();
    }
}
package org.miage.dosun.ui.windows.about;

import javax.swing.JFrame;
import javax.swing.JLabel;
import org.miage.dosun.ConstHelper;
import org.miage.dosun.helper.MethodHelper;
import org.miage.dosun.ui.extensions.Frame;
import org.miage.dosun.ui.extensions.Panel;
import java.awt.FlowLayout;
import java.io.IOException;

public class Apropos extends Frame{
	private static final long serialVersionUID = -7927241619111148058L;
	private static volatile Apropos instance;
	private JLabel content;
	private final String DevContent;
	private final String SoftContent;
	
	public Apropos() throws IOException{
		super("A propos");
		DevContent = MethodHelper.fileToString(ConstHelper.AssetsHtml + "/APropos_dev.html");
		SoftContent = MethodHelper.fileToString(ConstHelper.AssetsHtml + "/APropos_soft.html");
        Panel P = new Panel();
        content = new JLabel();
        P.setLayout(new FlowLayout());
        P.add(content);           
        add(P);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }
	
	public static Apropos Instance() throws IOException {
		if(instance == null) {
			synchronized(Apropos.class) {
				if(instance == null) {
					instance = new Apropos();
				}
			}
		}
		return instance;
	}
	
	public void Open(int n) {
		if (n == 1){
        	content.setText(DevContent);
            this.setSize(300,400);
        }
		else{
			content.setText(SoftContent);
            this.setSize(550,170);
		}
		setVisible(true);
	}
}
package org.miage.dosun.ui.extensions;
import javax.swing.JFrame;

public class Frame extends JFrame{
	private static final long serialVersionUID = 7345661995621386188L;

	public Frame(String title) {
		super();
		setTitle(title);
		setLocationRelativeTo(null);
        setResizable(false);
	}
}

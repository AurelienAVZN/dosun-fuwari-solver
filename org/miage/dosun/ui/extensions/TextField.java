package org.miage.dosun.ui.extensions;

import java.awt.Dimension;
import javax.swing.JTextField;

public class TextField extends JTextField{
	private static final long serialVersionUID = 6238614566294159688L;

	public TextField() {
		super("");
		this.setPreferredSize(new Dimension(100, 25));
	}
}

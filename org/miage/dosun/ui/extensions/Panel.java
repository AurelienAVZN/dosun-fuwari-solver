package org.miage.dosun.ui.extensions;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JPanel;
import org.miage.dosun.ConstHelper;

public class Panel extends JPanel{
	private static final long serialVersionUID = 3212341701514236835L;
	
	public Panel() {
		super();
		setBackground(ConstHelper.BackGroundColor);
	}

	public Panel(Dimension dimension) {
		super();
		setBackground(ConstHelper.BackGroundColor);
		setPreferredSize(dimension);
	}
	
	public void addComponent(Component... comps) {
		for(Component comp: comps) {
			add(comp);
		}
	}
}

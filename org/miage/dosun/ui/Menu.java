package org.miage.dosun.ui;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import org.miage.dosun.ui.helper.UIHelper;
import org.miage.dosun.ui.windows.about.Apropos;

public class Menu extends JMenuBar{
	private static final long serialVersionUID = 3283792522070527360L;
    private JMenu Menu1 = new JMenu("Fichier");
    private JMenu Menu2 = new JMenu("Edition");
    private JMenu Menu3 = new JMenu("Resoudre");
    private JMenu Menu4 = new JMenu("?");
    private JMenuItem ItemM11 = new JMenuItem("Nouveau");
    private JMenuItem ItemM12 = new JMenuItem("Ouvrir");
    private JMenuItem ItemM13 = new JMenuItem("Enregistrer");
    private JMenuItem ItemM14 = new JMenuItem("Enregistrer sous");
    private JMenuItem ItemM15 = new JMenuItem("Exit");
    private JMenuItem ItemM21 = new JMenuItem("Modifier taille");
    private JMenuItem ItemM22 = new JMenuItem("Modifier zones");
    private JMenuItem ItemM23 = new JMenuItem("Modifier CNoires");
    private JMenuItem ItemM31 = new JMenuItem("Creation Dimacs");
    private JMenuItem ItemM32 = new JMenuItem("Lancement du solveur");
    private JMenuItem ItemM33 = new JMenuItem("Récuperation de la solution");
    private JMenuItem ItemM41 = new JMenuItem("Regles du jeu");
    private JMenuItem ItemM42 = new JMenuItem("Format d'une Grille en .txt");
    private JMenuItem ItemM43 = new JMenuItem("A propos du Dosun-Fuwari");
    private JMenuItem ItemM44 = new JMenuItem("A propos du developpement");
    
    public Menu() {
    	InitAccelerator();
    	InitMouseListener();
    	DoAssociation();
    	Menu1.setMnemonic('F');
        Menu2.setMnemonic('E');
        Menu3.setMnemonic('R');
    }
    
    public JMenuItem ItemM11() {
    	return ItemM11;
    }
    
    public JMenuItem ItemM12() {
    	return ItemM12;
    }
    
    public JMenuItem ItemM31() {
    	return ItemM31;
    }
    
    public JMenuItem ItemM32() {
    	return ItemM32;
    }
    
    public JMenuItem ItemM33() {
    	return ItemM33;
    }
    
    /**
     * Permet de mettre en place les diff�rents raccourcis clavier pour appeler
     * les �l�ments du menu
     */
    private void InitAccelerator() {
    	ItemM11.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.VK_CONTROL));
        ItemM12.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.VK_CONTROL));
        ItemM13.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.VK_CONTROL));
        ItemM14.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK + KeyEvent.SHIFT_DOWN_MASK));
        ItemM21.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, KeyEvent.VK_CONTROL));
        ItemM22.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.VK_CONTROL));
        ItemM23.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.VK_CONTROL));
    }
    
    private void InitMouseListener() { 
    	ItemM12.addMouseListener(new java.awt.event.MouseAdapter(){
            public void mousePressed(java.awt.event.MouseEvent e){
                UIHelper.OpenFile();
            }
        });
    	
    	ItemM15.addMouseListener(new java.awt.event.MouseAdapter(){
            public void mousePressed(java.awt.event.MouseEvent e){
                System.exit(0);
            }
        });
    	
    	ItemM41.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
            //Appel le constructeur qui affiche les règles du jeu
                try{
                    Desktop d = Desktop.getDesktop();
                    d.open(new File("Divers/cours_eleve.pdf"));
                }catch (IOException e){
                    System.out.println("Failure" + e.toString());
                }
            }
        }); 
    	
    	ItemM42.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0){
                try{
                    Desktop d = Desktop.getDesktop();
                    d.open(new File("solveur.bat"));
                }catch (IOException e){
                    System.out.println("Failure" + e.toString());
                }
            }
        });
    	
    	ItemM43.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) OpenAbout(0);
            }
        });
    	
    	ItemM44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) OpenAbout(1);
            }
        });
    }
    
    private void OpenAbout(int n) {
    	try { Apropos.Instance().Open(n); }
    	catch(Exception e) { UIHelper.showErrorFrame("Error", e); }
    }
    
    private void DoAssociation() {
    	Menu1.add(ItemM11);
        Menu1.add(ItemM12);
        Menu1.add(ItemM13);
        Menu1.add(ItemM14);
        Menu1.add(ItemM15);
        Menu2.add(ItemM21);
        Menu2.add(ItemM22);
        Menu2.add(ItemM23);
        Menu3.add(ItemM31);
        Menu3.add(ItemM32);
        Menu3.add(ItemM33);
        Menu4.add(ItemM41);
        Menu4.add(ItemM42);
        Menu4.add(ItemM43);
        Menu4.add(ItemM44);
        add(Menu1); add(Menu2);
        add(Menu3); add(Menu4);
    }
}

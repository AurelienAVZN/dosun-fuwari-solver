package org.miage.dosun.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public final class MethodHelper {
	// Transforme du nombre s de string en int
	public static final int valNombre(final String s){
		int i = 0;	//curseur sur s 
		int result = 0;	//variable de stockage du resultat
		while(i != s.length()){
			result = result*10+(s.charAt(i)-'0');
			i++;
		}
		return result;
	}
	
	//retour 2^n
	public static int pow2(int n){
		int i = 1;
		while(n != 0){
			i = i*2;
			n--;
		}
		return i;
	}
	
	public static void DisposeSafe(AutoCloseable disposer) {
		if(disposer != null) {
			try { disposer.close(); } 
			catch (Exception e) { e.printStackTrace(); }
		}
	}
	
	public static String fileToString(String path) throws IOException {
		File file = new File(path);
		return !file.exists() ? null : fileToString(path, new StringBuffer((int) file.length()));
	}
	
	private static String fileToString(String path, StringBuffer builder) throws IOException {
		String line;
		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(new FileInputStream(path)))) {
			while((line = buffer.readLine()) != null) builder.append(line+"\n");
			return builder.toString();
		}
	}
}

package org.miage.dosun.patterns;

import java.util.Collection;

/**
 * Interface which extends the classical observer by giving it a list of
 * <code>IObservable</code> too in order to do specific stuffs in function of
 * the observable
 * 
 * @category Interface
 * @author Daeron (Avsoft Studio)
 * @see IObserver
 * @since 1.0
 * @version 1.0
 */
public interface IEObserver extends IObserver{

	/**
	 * @see IObserver#notifier()
	 * @since 1.0
	 */
	void notifier(IObservable observable);

	/**
	 * @param observable
	 * @since 1.0
	 */
	boolean addObservable(IObservable observable);

	/**
	 * @param observables
	 * @since 1.0
	 */
	boolean addObservables(Collection<? extends IObservable> observables);

	/**
	 * @return True if the <code>IObservables</code> is removed
	 * @since 1.0
	 */
	boolean removeObservable(IObservable observable);

	/**
	 * Remove the list o
	 * @param observables The list of <code>IObservable</code> to remove
	 * @return True if at least, one <code>IObservable</code> is removed from the
	 *         list
	 * @since 1.0
	 */
	boolean removeObservables(Collection<? extends IObservable> observables);

	/**
	 * Remove the <code>IObservale</code> at the index position of the list
	 * @param index The position of the <code>IObservable</code>
	 * @return The <code>IOservable</code> remove from the list
	 * @since 1.0
	 */
	IObservable removeObservable(int index);
}

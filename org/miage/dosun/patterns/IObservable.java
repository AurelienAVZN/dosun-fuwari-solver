package org.miage.dosun.patterns;

import java.util.Collection;

/**
 * Give the signature of the method notify for the observable in order 
 * to respect the design pattern Observer
 * @implNote Respect the design pattern Observer
 * @implSpec We add the possibility to notify just a few part of the observers
 * @author Daeron (Avsoft Studio)
 * @since 1.0
 * @version 1.0
 */
public interface IObservable {

    /**
     * 
     * @param index
     * @return
     * @since 1.0
     */
    IObserver removeObserver(int index);
    
	/**
     * Notify all the <code>IObserver</code> of the list given if these last
     * are in the list of the <code>IObservable</code>
     * @param observers The list of observers to notify
     * @implSpec Don't notify the <code>IObserver</code> if this last
     * aren't in the list of the <code>IObservable</code>
     * @since 1.0
     */
    void notifierHalf(Collection<? extends IObserver> observers);
    
    /**
     * Notify all the observers of the different changement made
     * by the observable
     * @since 1.0
     */
    void notifierAll();
    
    /**
     * Add an IObserver to the list of the IObservable
     * @param observer IObserver to add in the list
     * @since 1.0
     */
    boolean addObserver(IObserver observer);
    
    boolean addObserver(IEObserver observer);

    /**
     * 
     * @param observers
     * @since 1.0
     */
    boolean addObservers(Collection<? extends IObserver> observers);
    
    boolean addIEObservers(Collection<? extends IEObserver> observers);

    /**
     * 
     * @param observer
     * @return
     * @since 1.0
     */
    boolean removeObserver(IObserver observer);
    
    boolean removeObserver(IEObserver observer);

    /**
     * 
     * @param observers
     * @return
     * @since 1.0
     */
    boolean removeObservers(Collection<? extends IObserver> observers);
    
    boolean removeIEObservers(Collection<? extends IEObserver> observers);
}
